 var userChoice = 'denver'


var zoneOnly = {
    atlanta: 'Atlanta',
    austin: 'Austin',
    chicago: 'Chicago',
    dallas: 'Dallas',
    denver: 'Denver',
    houston: 'Houston',
    Miami: 'Miami',
    las_vegas: 'Las Vegas',
    los_angeles: 'Los Angeles',
    san_fransisco: 'San Fransisco'
}

var popularTrips = {
    austin: ['Las Vegas', 'San Fransisco', 'Denver'],
    atlanta: ['Chicago', 'New York', 'Denver'],
    chicago: ['Las Vegas','Los Angeles','Phoenix'],
    dallas: ['New York', 'Los Angeles', 'Denver'],
    denver: ['Los Angeles', 'Las Vegas', 'Austin'],
    houston: ['Orlando', 'Atlanta'],
    miami: ['Philadelphia', 'Atlanta', 'Las Vegas'],
    las_vegas: ['Denver', 'Austin', 'Los Angeles'],
    los_angeles: ['Austin', 'Las Vegas', 'San Fransisco'],
    san_fransisco: ['Denver', 'Las Vegas','Orlando']
}

var hotDestinations = {
    austin: ['New Orleans', 'Orlando', 'San Diego'],
    atlanta: ['New Orleans', 'Orlando', 'Los Angeles'],
    chicago: ['Orlando', 'Phoenix', 'Houston'],
    dallas: ['Phoenix', 'Orlando'],
    denver: ['New Orleans', 'Orlando', 'Austin'],
    houston: ['Los Angeles', 'Phoenix'],
    miami: ['Las Vegas', 'Orlando'],
    las_vegas: ['Los Angeles', 'New Orleans', 'Phoenix'],
    los_angeles: ['Las Vegas', 'New Orleans', 'Orlando'],
    san_fransisco: ['Phoenix', 'San Diego', 'Las Vegas']
}

var winterWeather = {
    austin: ['Denver', 'Salt Lake City', 'Chicago'],
    atlanta: ['Denver', 'Salt Lake City', 'Chicago'],
    dallas: ['Denver'],
    denver: ['Salt Lake City', 'Chicago'],
    houston: ['Denver'],
    las_vegas: ['Denver', 'Salt Lake City', 'Chicago'],
    los_angeles: ['Denver', 'Salt Lake City', 'Chicago'],
    san_fransisco: ['Denver']
}

var zone = {
    "atlanta": [
        "Austin",
        "Chicago",
        "Dallas",
        "Denver",
        "Detroit",
        "Houston",
        "Las Vegas",
        "Los Angeles",
        "Minneapolis",
        "New Orleans",
        "New York",
        "Orlando",
        "Philadelphia",
        "Salt Lake City",
        "San Fransisco"
    ],
    "austin": [
        "Atlanta",
        "Chicago",
        "Charlotte",
        "Dallas",
        "Denver",
        "Detroit",
        "Indianapolis",
        "Las Vegas",
        "Los Angeles",
        "Milwaukee",
        "Minneapolis",
        "New Orleans",
        "New York",
        "Orlando",
        "Philadelphia",
        "Salt Lake City",
        "San Diego",
        "Seattle"
    ],
    "chicago": [
      "Atlanta",
      "Charlotte",
      "Dallas",
      "Denver",
      "Detroit",
      "Houston",
      "Las Vegas",
      "Los Angeles",
      "New York",
      "Orlando",
      "Phoenix",
      "San Diego",
    ],
    "dallas": [
      "Atlanta",
      "Austin",
      "Chicago",
      "Denver",
      "Houston",
      "Los Angeles",
      "New York",
      "Oklahoma City",
      "Orlando",
      "Philadelphia",
      "Phoenix",
      "San Fransisco",
    ],
    "denver": [
        "Atlanta",
        "Austin",
        "Chicago",
        "Charlotte",
        "Dallas",
        "Detroit",
        "Houston",
        "Kansas City",
        "Las Vegas",
        "Los Angeles",
        "Minneapolis",
        "New Orleans",
        "New York",
        "Oklahoma City",
        "Orlando",
        "Philadelphia",
        "Phoenix",
        "Salt Lake City",
        "San Antonio",
        "San Diego",
        "San Fransisco",
        "Seattle"
    ],
    "houston": [
        "Atlanta",
        "Chicago",
        "Denver",
        "Detroit",
        "Las Vegas",
        "Los Angeles",
        "Orlando",
        "Phoenix",
    ],
    "miami": [
        "Atlanta",
        "Chicago",
        "Dallas",
        "Denver",
        "Detroit",
        "Houston",
        "Las Vegas",
        "Los Angeles",
        "New York",
        "Orlando",
        "Philadelphia",
    ],
    "las_vegas": [
        "Atlanta",
        "Chicago",
        "Dallas",
        "Denver",
        "Houston",
        "Los Angeles",
        "New Orleans",
        "New York",
        "Philadelphia",
        "Phoenix",
        "Salt Lake City",
        "San Diego",
        "San Fransisco",
        "Seattle"
    ],
    "los_angeles": [
        "Atlanta",
        "Austin",
        "Chicago",
        "Dallas",
        "Denver",
        "Houston",
        "Las Vegas",
        "New Orleans",
        "New York",
        "Orlando",
        "Philadelphia",
        "Phoenix",
        "Salt Lake City",
        "San Fransisco",
        "Seattle"
    ],
    "san_fransisco": [
        "Atlanta",
        "Austin",
        "Chicago",
        "Dallas",
        "Denver",
        "Houston",
        "Las Vegas",
        "Los Angeles",
        "Miami",
        "New Orleans",
        "Orlando",
        "Philadelphia",
        "Phoenix",
    ],
}

jQuery(function() {

    data = '<option value="">Select Departure</option>';
    $('#departure').append(data);

    for (x in zoneOnly) {

        //data = '<option value="'+x+'">'+zoneOnly[x]+'</option>';
      //  $('#departure').append(data);
      //  $('#arrival').append(data);

    }

    $('#departure').change(function() {
       for(x in zone) {

        if(zone[x] == $('#departure').val()) {

            for(var i = 0; i < x.zone[x].length ; i++) {
                data = '<option value="'+zone[x][i]+'">'+zone[x][i]+'</option>';

            }


        }
       }
    });

});
