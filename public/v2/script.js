var arrival = document.getElementById('arrival')
var departure = document.getElementById('departure')
var emailInput = document.getElementById('bookingemail');
var popularTripsHeading = document.getElementById('popular-trips')
var hotSpotsHeading = document.getElementById('hot-destinations')
var winterWeatherHeading = document.getElementById('winter-weather')

//card variables
var tripCard = document.getElementById('card-trip')
var hotCard = document.getElementById('card-hot')
var winterCard = document.getElementById('card-winter')

//welcome data variable
var username = document.getElementById('user-name');
var availableTokens = document.getElementById('available-tokens');

var tripCardArray;

var formData = {}



// firebase calls
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // console.log(user)
        getUserDetails()
        emailInput.value = user.email
    }
});

function getUserDetails(uid) {
    var userId = firebase.auth().currentUser.uid;

    firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {

        var subscriberMarket = (snapshot.val() && snapshot.val().subscriber_market);
        var firstName = (snapshot.val() && snapshot.val().firstname);
        var lastName = (snapshot.val() && snapshot.val().lastname);
        var tokens = (snapshot.val() && snapshot.val().tokens.available);

        onLoadPage(subscriberMarket);
        username.innerHTML = firstName + ' ' + lastName
        availableTokens.innerHTML = tokens
    });

}

function escapeEmailAddress(email) {
    if (!email) return false

    // Replace '.' (not allowed in a Firebase key) with ',' (not allowed in an email address)
    email = email.toLowerCase();
    email = email.replace(/\./g, ',');
    return email;
}


//other page only functions
departure.addEventListener('change', function () {

    var selection = departure.options[departure.selectedIndex].value
    var selectedText = departure.options[departure.selectedIndex].innerText

    if (selection) {
        //searching through keys and build HTML
        keyAndBuild(selection, iterateOver())
        //building card for three sectios
        buildCardsFromData(selection, tripCard, popularTrips, iterateOver())
        buildCardsFromData(selection, hotCard, hotDestinations, iterateOver())
        buildCardsFromData(selection, winterCard, winterWeather, iterateOver())
        setEventListener()
        //changing heading of sections
        popularTripsHeading.innerHTML = selectedText;
        hotSpotsHeading.innerHTML = selectedText;
        winterWeatherHeading.innerHTML = selectedText;

    }
})



function buildDepartureList(userChoice) {
    for (let [key, value] of Object.entries(zoneOnly)) {
        //
        if (key == userChoice) {
            departure.innerHTML += `<option value=${key} selected>${value}</option>`
        } else {
            departure.innerHTML += `<option value=${key}>${value}</option>`
        }

    }
}

var someZone;

//crate option elemts from array
function keyAndBuild(city, callback) {
    var re = new RegExp(city, "g");
    arrival.innerHTML = ''
    // tripCard.innerHTML = ''
    for (var key in zone) {
        if (zone.hasOwnProperty(key)) {
            if (re.test(key)) {
                someZone = zone[key]
                zone[key].forEach(function (element) {
                    //creating options dynamically
                    arrival.innerHTML += buildSelection(element)
                    // tripCard.innerHTML += buildCards(element)
                });
            }
        }
    }
    callback
}


//Creating cards from Array with given city
function buildCardsFromData(city, container, placesArray, callback) {
    var re = new RegExp(city, "g");
    container.innerHTML = ''
    for (var key in placesArray) {
        if (placesArray.hasOwnProperty(key)) {
            if (re.test(key)) {
                placesArray[key].forEach(function (element) {
                    //creating options dynamically
                    container.innerHTML += buildCards(element)
                });
            }
        }
    }
    callback
}

//a function that creates option elemnts for booking engine form
function buildSelection(data) {
    return `<option>${data}</option>`
}

//a function that creates cards from the data provided 
function buildCards(data) {
    var dataCity = safeObjectString(data)
    return `<div data-city=${dataCity} class='col-sm trip-card' style='background-image: url(/images/${dataCity}.jpeg);'>
  <h1>${data}</h1>
</div>`
}

//safe string creator for object searcg
function safeObjectString(string) {
    var stringToLowerCase = string.toLowerCase()
    var splitted = stringToLowerCase.split(" ");
    var arrayLength = splitted.length

    var newKey = ''
    for (i = 0; i < arrayLength; i++) {

        if (i > 0 && i < arrayLength) {
            newKey += '_' + splitted[i]
        } else {
            newKey += splitted[i]
        }
    }
    return newKey
}



//creating an Array of HTML elements with same classes.
function iterateOver() {
    tripCardArray = document.getElementsByClassName('trip-card');
}

var knowMyCity = function () {
    var attribute = this.getAttribute("data-city");
    var itemText = this.innerText
    for (var i = 0; i < tripCardArray.length; i++) {
        tripCardArray[i].style.border = 'none';

    }
    this.style.border = "thick solid #0A93DD";
    arrival.options[arrival.selectedIndex].innerText = itemText


};

function changeArrivalDropDown(value) {
    //some functions for future
}


//This code runs during intial loading of the page
function onLoadPage(userChoice) {
    keyAndBuild(userChoice, iterateOver())
    buildCardsFromData(userChoice, tripCard, popularTrips, iterateOver())
    buildCardsFromData(userChoice, hotCard, hotDestinations, iterateOver())
    buildCardsFromData(userChoice, winterCard, winterWeather, iterateOver())
    buildDepartureList(userChoice)

    //change heading of sections
    popularTripsHeading.innerHTML = toTitleCase(userChoice);
    hotSpotsHeading.innerHTML = toTitleCase(userChoice);
    winterWeatherHeading.innerHTML = toTitleCase(userChoice);
    setEventListener()
}



//Setting Click event listener for each card items that belongs to same class
function setEventListener() {
    for (var i = 0; i < tripCardArray.length; i++) {
        tripCardArray[i].addEventListener('click', knowMyCity, false);
    }
}


//A function that collects data from html ements to formData Object
function bookNow() {
    formData.departure = departure.value;
    formData.arrival = arrival.value;
    formData.date = document.getElementById('bookingdate').value;
    formData.email = emailInput.value;
}

//converting text to TitleCase function
function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}


