function getFormData() {
  let elements = document.getElementById('panel-form').elements;
  let result = {};

  for (let i = 0; i < elements.length; i++) {
    let item = elements.item(i);
    if (item.name != "") {
      result[item.name] = item.value;
    }
  }

  console.log('getFormData.result', result);

  return result;
}

function makeRandomString() {
  let result = "";
  let possible = "abcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 10; i++) {
    result += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return result;
}

function book() {
  let formData = getFormData();
  console.log(formData);

  var firebaseRef = firebase.database().ref();

  let requestId = makeRandomString();
  var res = firebaseRef.child("Booking Request " + requestId).set(formData);
  console.log(res);

  return false;
}
