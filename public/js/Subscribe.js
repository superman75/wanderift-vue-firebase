function getFormData() {
  let elements = document.getElementById('panel-form').elements;
  let result = {};

  for (let i = 0; i < elements.length; i++) {
    let item = elements.item(i);
    if (item.name != "") {
      result[item.name] = item.value;
    }
  }

  console.log('getFormData.result', result);

  return result;
}

function makeRandomString() {
  let result = "";
  let possible = "abcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 10; i++) {
    result += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return result;
}

function Send() {
  let formData = getFormData();

  var firebaseRef = firebase.database().ref();

  let requestId = makeRandomString();
  firebaseRef.child("Wait List " + requestId).set(formData);

  return false;
}
