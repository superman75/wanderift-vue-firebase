var currentUser = firebase.auth().currentUser;

var db = firebase.database();

var currentMonth = moment().format('MMM');



var auth = firebase.auth();

var forgotPasswordButton = document.getElementById('forgot-password');



forgotPasswordButton.addEventListener('click', function () {
  forgotPassword()
})


function forgotPassword() {
  var userEmail = document.getElementById("email_field").value;
  auth.sendPasswordResetEmail(userEmail).then(function () {
    // Email sent.
    alert('password reset link has been sent to your email ' + userEmail)
  }).catch(function (error) {
    alert(error)
  });
}

function getEmailID() {
  console.log(userEmail)
}


firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    // User is signed in.

    if (user != null) {

      updateToken(user.uid)
      console.log('user is', user.uid)
      window.location = 'UserHome.html';
    }
  } else {

  }
});

function login() {
  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;


  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function (error) {

    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);

  });

}

function updateToken(uid) {
  //checking available tokens
  firebase.database().ref('/users/' + uid).once('value').then(function (snapshot) {

    var tokens = (snapshot.val() && snapshot.val().tokens.available);
    var lastupdated = (snapshot.val() && snapshot.val().tokens.lastupdated);

    if (lastupdated != currentMonth) {
      if (tokens < 5) {
        var newToken = tokens + 1;
        var postData = { available: newToken, lastupdated: currentMonth };
        var updates = {};
        updates['/users/' + uid + '/tokens/'] = postData;
        firebase.database().ref().update(updates);
      }
    } else {
      console.log('No changes made')
    }

  });

}
