
var db = firebase.database();
var currentMonth = moment().format('MMM');

function getFormData() {
  let elements = document.getElementById('booking-form').elements;
  let result = {};

  for (let i = 0; i < elements.length; i++) {
    let item = elements.item(i);
    if (item.name != "") {
      result[item.name] = item.value;
    }
  }

  return result;
}

function makeRandomString() {
  let result = "";
  let possible = "abcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 10; i++) {
    result += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return result;
}

function book() {
  let formData = getFormData();
  var today = Date.now()
  //get data from user




  //checking user login status
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      //get data from the user
      firebase.database().ref('/users/' + user.uid).once('value').then(function (snapshot) {

        var tokens = (snapshot.val() && snapshot.val().tokens.available);

        if (tokens > 0) {
          showSuccessModal()
          //Make booking
          db.ref('bookings/' + user.uid + '/' + today).set(formData);

          //deducting token from available tokens
          var newToken = tokens - 1;
          var postData = { available: newToken, lastupdated: currentMonth };
          var updates = {};
          //updating data 
          updates['/users/' + user.uid + '/tokens/'] = postData;
          firebase.database().ref().update(updates);

        } else {
          NoTokenModal()
          console.log('no token left')
        }
      });
    }
  })

}

function showSuccessModal() {
  //show pop up
  $('#popup-modal').modal('show');
}

function NoTokenModal() {
  //show pop up
  $('#no-token-modal').modal('show');
}


