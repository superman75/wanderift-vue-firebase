function logout() {
    console.log('Account');
    firebase.auth().signOut();
    window.location.href = "Login.html";
}

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        console.log(user)
        getUserDetails()
    }
});

function getUserDetails() {
    var userId = firebase.auth().currentUser.uid;

    firebase.database().ref('/users/' + userId).once('value').then(function (snapshot) {
        //collecting data to user profile object 
        var userProfile = {}

        userProfile.firstName = (snapshot.val() && snapshot.val().firstname);
        userProfile.lastName = (snapshot.val() && snapshot.val().lastname);
        userProfile.dob = (snapshot.val() && snapshot.val().dob);
        userProfile.email = (snapshot.val() && snapshot.val().email);
        userProfile.gender = (snapshot.val() && snapshot.val().gender);
        userProfile.market = (snapshot.val() && snapshot.val().subscriber_market);
        userProfile.tsrp = (snapshot.val() && snapshot.val().tsa_number);

        //writing html files with collected data
        writeToHtml(userProfile)


    });

}
// a function that writes data to html elements
function writeToHtml(user) {
    document.getElementById('first-name').innerHTML = user.firstName
    document.getElementById('last-name').innerHTML = user.lastName
    document.getElementById('dob').innerHTML = user.dob
    document.getElementById('email').innerHTML = user.email
    document.getElementById('gender').innerHTML = user.gender
    document.getElementById('sub-market').innerHTML = user.market
    document.getElementById('tsrp').innerHTML = user.tsrp
}