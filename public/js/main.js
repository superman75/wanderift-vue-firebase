$(function () {
	"use strict";


	/* ==========================================================================
       Destinations Slider
       ========================================================================== */




	// console.log('loaded');
	let slider = $('.pww-slider-init').slick({
		slidesToShow: 3,
		slidesToScroll: 2,
		infinite:true,
		speed:800,
		dots: false,
		arrows: false,
		autoplay:true,
		autoplaySpeed: 2500,
		responsive: [{
			breakpoint: 776,
			settings : {
				slidesToShow: 3,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 576,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});



	function sliderAnimator() {

		let selected = $(this).val();

		if(selected == '' || selected == null) {
			selected = "Atlanta";
		}
		// console.log(selected);
		$('div.pww-slider-init').each(function() {
			$(this).fadeTo(600,0).css('display','none');

		});
		$('.slider-loading').css('display','block');

		$('div.pww-slider-init[slider-bind="'+selected+'"]').css('display','block').fadeTo(1000,1,function() {
			$('.slider-loading').css('display','none');
		});
		slider.slick('setPosition');

	let	tag_name = selected;

		if(tag_name == 'Las_Vegas') {
			tag_name = 'Las Vegas';
		}

		if(tag_name == 'Los_Angeles') {
			tag_name = 'Los Angeles';
		}

		if(tag_name == 'San_Fransisco') {
			tag_name = 'San Fransisco';
		}



		$('.pww-slider .dest-tagline .destination-name').html(tag_name);

	}
		sliderAnimator();



	$('.arrow-next').click(function() {
		slider.slick('slickNext');
	});

	$('.arrow-prev').click(function() {
		slider.slick('slickPrev');
	});

	$('select[name="pww-slider-selector"]').change(sliderAnimator);





	/* ==========================================================================
       Sub Form
       ========================================================================== */


	$('#mc-form').ajaxChimp({
		callback: callbackFunction,
		url: 'http://csmthemes.us3.list-manage.com/subscribe/post?u=9666c25a337f497687875a388&id=45b2c44b98'
			// http://xxx.xxx.list-manage.com/subscribe/post?u=xxx&id=xxx
	});


	function callbackFunction(resp) {
		if (resp.result === 'success') {
			$('#mc-error').slideUp();
			$('#mc-success').slideDown();
			$('#mc-form button').prop('disabled', true);
		} else if (resp.result === 'error') {
			$('#mc-success').slideUp();
			$('#mc-error').slideDown();
		}
	}




	/* ==========================================================================
       Navbar
     ========================================================================== */

	$(window).on('scroll', function () {
		var scrollValue = $(window).scrollTop();
		if (scrollValue > 400) {
			$('.navbar').addClass('affix');
		} else {
			$('.navbar').removeClass('affix');
		}
	});



	/* ==========================================================================
	   Collapse nav bar
	   ========================================================================== */


	$(".nav-item a:not(.nav-item .dropdown-toggle)").on('click', function () {
		$(".navbar-collapse").collapse('hide');
		$(".navbar-toggler").removeClass("hamburger-active");
	});



	/* ==========================================================================
	   Dropdown menu easing
	   ========================================================================== */


	$('.dropdown').on('show.bs.dropdown', function () {
		$(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
	});


	$('.dropdown').on('hide.bs.dropdown', function () {
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
	});




	/* ==========================================================================
	   Hamburger Menu Animation
	   ========================================================================== */



	$(".navbar-toggler").on("click", function () {
		$(this).toggleClass("hamburger-active");
	});







	/* ==========================================================================
       Smooth scroll
     ========================================================================== */


	$('a[href*="#"]')

	.not('[href="#"]')
		.not('[href="#0"]')
		.on('click', function (event) {

			if (
				location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') &&
				location.hostname === this.hostname
			) {

				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

				if (target.length) {

					event.preventDefault();
					$('html, body').animate({
						scrollTop: (target.offset().top - 80)
					}, 1000);
					return false;
				}
			}

		});





	/* ==========================================================================
	Contact form
	========================================================================== */


	var formFields = $('.contact-form form input, .contact-form form textarea');



	$(formFields).on('focus', function () {
		$(this).removeClass('input-error');
	});
	$('.contact-form form').submit(function (e) {
		e.preventDefault();
		$(formFields).removeClass('input-error');
		var postdata = $('.contact-form form').serialize();
		$.ajax({
			type: 'POST',
			url: 'php/contact.php',
			data: postdata,
			dataType: 'json',
			success: function (json) {

				if (json.nameMessage !== '') {
					$('.contact-form form .contact-name').addClass('input-error');
				}
				if (json.emailMessage !== '') {
					$('.contact-form form .contact-email').addClass('input-error');
				}
				if (json.messageMessage !== '') {
					$('.contact-form form textarea').addClass('input-error');
				}
				if (json.antispamMessage !== '') {
					$('.contact-form form .contact-antispam').addClass('input-error');
				}
				if (json.nameMessage === '' && json.emailMessage === '' && json.messageMessage === '' && json.antispamMessage === '') {

					$('.contact-form-success').slideDown();


					$('.contact-form form button').prop('disabled', true);
					$('.contact-form form').find('input, textarea').val('');

				}
			}
		});
	});




});
