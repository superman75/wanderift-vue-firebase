const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG);
const stripeKey ="sk_live_Um4ThSBBFqvAlZkBkPzIBr5O"
const stripe = require('stripe')(stripeKey);
exports.createStripeCustomer = functions.auth.user().onCreate(event => {
    // user auth data
    const user = event.data;

    // register Stripe user
    return stripe.customers.create({
            email: user.email
        })
        .then(customer => {
            /// update database with stripe customer id

            const data = { customerId: customer.id }

            const updates = {}
            updates[`/customers/${customer.id}`] = user.uid
            updates[`/users/${user.uid}/customerId`] = customer.id


            return admin.database().ref().update(updates);
        });
});
exports.cancelSubscription = functions.database.ref('/server-process/cancel/{userId}').onCreate(event => {
    // user auth data
    const user = event.params.userId;

    return admin.database()
        .ref(`/users/${user}/pro-membership/sub_id`)
        .once('value')
        .then(snapshot => snapshot.val())
        .then((subId) => {
            console.log(subId)
            stripe.subscriptions.del(
                subId,
                function(err, confirmation) {
                    console.log(err)
                    console.log(confirmation)
                    return Promise.all([admin.database()
                        .ref(`/users/${user}/pro-membership`)
                        .update({ status: 'cancel' }), event.data.ref.remove()
                    ])
                }
            );

        })
});
exports.createSubscription = functions.database.ref('/users/{userId}/pro-membership/token').onWrite(event => {

    const tokenId = event.data.val();
    const userId = event.params.userId;


    if (!tokenId) throw new Error('token missing');

    return admin.database()
        .ref(`/users/${userId}`)
        .once('value')
        .then(snapshot => snapshot.val())
        .then(user => {
            console.log(user)
            return stripe.subscriptions.create({
                customer: user.customerId,
                source: tokenId,
                items: [{
                    plan: 'plan_E7OyAeLlMdeP0b',
                }, ],
            });

        })
        .then(sub => {
            admin.database()
                .ref(`/users/${userId}/pro-membership`)
                .update({ status: 'active', sub_id: sub.id })


        })
        .catch(err => console.log(err))

});
exports.recurringPayment = functions.https.onRequest((req, res) => {

    const hook = req.body.type
    const data = req.body.data.object

    if (!data) throw new Error('missing data')

    return admin.database()
        .ref(`/customers/${data.customer}`)
        .once('value')
        .then(snapshot => snapshot.val())
        .then((userId) => {
            const ref = admin.database().ref(`/users/${userId}/pro-membership`)

            // Handle successful payment webhook
            if (hook === 'invoice.payment_succeeded') {
                return ref.update({ status: 'active' });
            }

            // Handle failed payment webhook
            if (hook === 'invoice.payment_failed') {
                return ref.update({ status: 'pastDue' });
            }


        })
        .then(() => res.status(200).send(`successfully handled ${hook}`))
        .catch(err => res.status(400).send(`error handling ${hook}`))


});

/*express = require("express");
var app = express();
app.listen(3030, function(){
console.log("running");
});*/
